import types from './types'

const mutations = {
  [types.ADD_PRODUCT] (state, { product }) {
    state.all.push(product);
  },

  [types.REMOVE_PRODUCTS] (state) {
    state.all = [];
  }
}

export default mutations