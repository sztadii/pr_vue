import types from './types'

const actions = {
  addProduct({ commit }, product) {
    commit(types.ADD_PRODUCT, { product })
  },
  removeAll({ commit }) {
    commit(types.REMOVE_PRODUCTS)
  }
}

export default actions